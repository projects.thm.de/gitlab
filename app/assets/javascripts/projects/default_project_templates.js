import { s__ } from '~/locale';

export default {
  sample: {
    text: s__('ProjectTemplates|Sample GitLab Project'),
    icon: '.template-option .icon-sample',
  },
  plainhtml: {
    text: s__('ProjectTemplates|Plain HTML'),
    icon: '.template-option .icon-plainhtml',
  },
  jekyll: {
    text: s__('ProjectTemplates|Jekyll'),
    icon: '.template-option .icon-jekyll',
  },
  kaniko: {
    text: s__('ProjectTemplates|Kaniko'),
    icon: '.template-option .icon-kaniko',
  },
  inara: {
    text: s__('ProjectTemplates|JOSS/Inara'),
    icon: '.template-option .icon-inara',
  },
  bridgetown: {
    text: s__('ProjectTemplates|Pages/Bridgetown'),
    icon: '.template-option .icon-gitlab_logo',
  },
  typo3_distribution: {
    text: s__('ProjectTemplates|TYPO3 Distribution'),
    icon: '.template-option .icon-typo3',
  },
  laravel: {
    text: s__('ProjectTemplates|Laravel Framework'),
    icon: '.template-option .icon-laravel',
  },
  astro_tailwind: {
    text: s__('ProjectTemplates|Astro Tailwind'),
    icon: '.template-option .icon-gitlab_logo',
  },
};
