# frozen_string_literal: true

module ProjectTemplateTestHelper
  def all_templates
    %w[
        plainhtml jekyll kaniko inara
      ]
  end
end

ProjectTemplateTestHelper.prepend_mod
