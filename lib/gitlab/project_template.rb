# frozen_string_literal: true

module Gitlab
  class ProjectTemplate
    attr_reader :title, :name, :description, :preview, :logo

    def initialize(name, title, description, preview, logo = 'illustrations/gitlab_logo.svg')
      @name = name
      @title = title
      @description = description
      @preview = preview
      @logo = logo
    end

    def file
      archive_path.open
    end

    def archive_path
      self.class.archive_directory.join(archive_filename)
    end

    def archive_filename
      "#{name}.tar.gz"
    end

    def clone_url
      "#{preview}.git"
    end

    def project_host
      return unless preview

      uri = URI.parse(preview)
      uri.path = ""
      uri.to_s
    end

    def project_path
      URI.parse(preview).path.delete_prefix('/')
    end

    def uri_encoded_project_path
      ERB::Util.url_encode(project_path)
    end

    def ==(other)
      name == other.name && title == other.title
    end

    class << self
      # TODO: Review child inheritance of this table (see: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/41699#note_430928221)
      # rubocop:disable Metrics/AbcSize
      def localized_templates_table
        [
          ProjectTemplate.new('plainhtml', 'Plain HTML', _('A template for GitLab Pages using plain HTML, CSS and JavaScript.'), 'https://git.thm.de/howto/templates/html'),
          ProjectTemplate.new('jekyll', 'Jekyll', _('A template for GitLab Pages using Jekyll.'), 'https://git.thm.de/howto/templates/jekyll'),
          ProjectTemplate.new('kaniko', 'Kaniko', _('Build docker images in your CI pipeline using Kaniko.'), 'https://git.thm.de/howto/templates/kaniko'),
          ProjectTemplate.new('inara', 'JOSS/Inara', _('Build JOSS pdf documents from Markdown using Inara.'), 'https://git.thm.de/howto/templates/inara')
        ].freeze
      end
      # rubocop:enable Metrics/AbcSize

      def all
        localized_templates_table
      end

      def find(name)
        all.find { |template| template.name == name.to_s }
      end

      def archive_directory
        Rails.root.join("vendor/project_templates")
      end
    end
  end
end

Gitlab::ProjectTemplate.prepend_mod_with('Gitlab::ProjectTemplate')
